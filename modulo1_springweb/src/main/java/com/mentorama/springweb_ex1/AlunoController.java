package com.mentorama.springweb_ex1;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/alunos")
public class AlunoController {

    private List<Aluno> alunos;

    public AlunoController() {
        alunos = new ArrayList<>();
    }

    @GetMapping
    public List<Aluno> findAll(@RequestParam(required = false) String nome,
                               @RequestParam(required = false) Integer idade) {
        if(nome != null) {
            return alunos.stream()
                    .filter(aluno -> aluno.getNome().contains(nome))
                    .collect(Collectors.toList());
        }

        if(idade != null) {
            return alunos.stream()
                    .filter(aluno -> aluno.getIdade().equals(idade))
                    .collect(Collectors.toList());
        }

        return alunos;
    }

    @GetMapping("/{id}")
    public Aluno findById(@PathVariable Integer id) {
        return alunos.stream()
                .filter(aluno -> aluno.getId().equals(id))
                .findFirst()
                .orElse(null);
    }

    @PostMapping
    public ResponseEntity<Integer> save(@RequestBody Aluno aluno) {
        if(aluno.getId() == null) {
            aluno.setId(alunos.size() + 1);
        }
        alunos.add(aluno);
        return new ResponseEntity(aluno.getId(), HttpStatus.CREATED);
    }

    @PutMapping
    public ResponseEntity update(@RequestBody Aluno aluno) {
        alunos.stream()
                .filter(aln -> aln.getId().equals(aluno.getId()))
                .forEach(aln -> {
                    aln.setNome(aluno.getNome());
                    aln.setIdade(aluno.getIdade());
                });
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable Integer id) {
        alunos.removeIf(aln -> aln.getId().equals(id));
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }
}
